import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from './catalog/catalog.component';
import { ConfiguratorComponent } from './configurator/configurator.component';
import { DeploymentComponent } from './deployment/deployment.component';

const routes: Routes = [
  { path: '', component: CatalogComponent},
  { path: 'configurator', component: ConfiguratorComponent},
  { path: 'deployment', component: DeploymentComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
