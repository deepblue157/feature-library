export interface FeatureData {
    name: string;
    description: string;
    dimension: string;
    format: string;
    class: string;
  }
  
  export const Feature_Data: FeatureData[] = [
    {
      name: "Feature_1",
      description: "Feature Description 1",
      dimension: "member",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    {
      name: "Feature_2",
      description: "Feature Description 2",
      dimension: "claim",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    {
      name: "Feature_3",
      description: "Feature Description 3",
      dimension: "provider",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    {
      name: "Feature_4",
      description: "Feature Description 4",
      dimension: "claim",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    {
      name: "Feature_6",
      description: "Feature Description 1",
      dimension: "provider",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    {
      name: "Feature_5",
      description: "Feature Description 1",
      dimension: "member",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    {
      name: "Feature_7",
      description: "Feature Description 1",
      dimension: "member",
      format: "int",
      class: "feature"//,
    //   "status": "",
    //   "repoBranch": [
    //     "master",
    //     "feature-branch-1",
    //     "feature-branch-2"
    //   ],
    //   "repoURL": "https://gitlab.ABC.com/Platform/Library",
    //   "requestedBy": "Mike.Brown",
    //   "dateRequested": "2017-09-11",
    //   "developedBy": "Joe.Smith",
    //   "dateReleased": "",
    //   "releasedModels": [  
    //   ],
    //   "releasedClients": [    
    //   ],
    //   "calcLogic": [
    //     {
    //       description: "CalcLogic Description 1",
    //       "code": "Calculate the values XYZ = 1 based on the table on the right.",
    //       "snapShotDate": [
    //         {
    //           "date": "2016-11-23",
    //           "fieldName": "Field_A1",
    //           description: "Description A1"
    //         }
    //       ],
    //       "rollUpInd": "Y",
    //       "rollUp": [
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(cust_claim_id, paid_date <= current_date)"
    //         },
    //         {
    //           "groupByField": "cust_claim_id",
    //           "transformation": "rollup_sum(memberId, call_date>= (current_date-12 months))"
    //         }
    //       ]
    //     }
    //   ],
    //   "calcInputReq": [
    //     {
    //       "targetFunctions": [
    //         "ABC_OPS",
    //         "ABC_XYZ"
    //       ],
    //       "sourceData": [
    //         {
    //           "table": "Claim_header",
    //           "columns": [
    //             "cust_claim_id",
    //             "paid_date"
    //           ]
    //         },
    //         {
    //           "table": "T_Member_calls_history",
    //           "columns": [
    //             "call_date"
    //           ]
    //         },
    //         {
    //           "table": "T_group_history",
    //           "columns": [
    //             "groupnm",
    //             "subgroupnm",
    //             "EnrMBUCd"
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    },
    ];