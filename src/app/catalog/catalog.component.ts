import {SelectionModel} from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import {Feature_Data} from '../featureData';
import {FeatureData} from '../featureData';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {
  //@ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ['select', 'name', 'description', 'dimension', 'format'];
  dataSource = new MatTableDataSource<FeatureData>(Feature_Data);
  selection = new SelectionModel<FeatureData>(true, []);
  
  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
 

  constructor() { }

  ngOnInit() {
    //this.dataSource.sort = this.sort;
  }

}
